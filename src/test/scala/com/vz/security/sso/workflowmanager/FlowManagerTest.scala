//package com.vz.security.sso.workflowmanager
//
//import akka.actor.{Actor, ActorSystem, Props}
//import akka.stream.ActorMaterializer
//import akka.testkit.TestKit
//import com.vz.security.sso.BaseSpec
//import com.vz.security.sso.grpc.services.ReplicationRequest
//import com.vz.security.sso.models.GenericMapRequest
//import com.vz.security.sso.utils.ObjectMapperUtil
//import org.scalatest.FunSuite
//import org.scalatest.time.{Millis, Seconds, Span}
//
//import scala.concurrent.{Await, ExecutionContext}
//
//class FlowManagerTest extends TestKit(ActorSystem("FlowManagerTestSystem")) with BaseSpec {
//
//  val wfm = WorkFlowManager(Some(etniActorRef), Some(vcvsActorRef))
//
//  val newWF = WorkFlowGraph(FlowManagerTestData.ivrClientWF, wfm.gatePassMap)
//
//  WorkFlowManager.workFlowMap += (FlowManagerTestData.ivrClientWF.module -> newWF)
//
//  val etniActorRef = system.actorOf(Props(new ETNITestActor(wfm)), name = "ETNIActor")
//  val vcvsActorRef = system.actorOf(Props(new VCVSTestActor(wfm)), name = "VCVSActor")
//
//  implicit val actorSystem: ActorSystem = system
//  implicit val ec: ExecutionContext = system.dispatcher
//  implicit val mat: ActorMaterializer = ActorMaterializer()
//
//  implicit val defaultPatience: PatienceConfig = PatienceConfig(timeout = Span(60, Seconds), interval = Span(500, Millis))
//
//  override def afterEach: Unit = {
//    println()
//  }
//
//  override def afterAll: Unit = {
//    TestKit.shutdownActorSystem(system)
//  }
//
//
//  "FlowManager" should "execute chain gateway -> etni" in {
//    val right = wfm.callWorkFlow(FlowManagerTestData.ivrClientWF.module, Map("dnis" -> "2008", "omdnStatus" -> "ai", "result" -> "yes")) match {
//      case Right(x) =>
//        x
//    }
//    whenReady(right) {
//      status =>
//        println("RESULT " + status)
//        assert(status == "Message reached ETNI")
//    }
//  }
//}
//
//private class ETNITestActor(wfm: WorkFlowManager) extends Actor {
//  def receive = {
//    case replicatedRequest: GenericMapRequest =>
//      wfm.callWorkFlow(FlowManagerTestData.ivrClientWF.module, replicatedRequest.fields, sender())
//    case m@_ => println("ETNITestActor: Unsupported request type " + m)
//  }
//}
//
//private class VCVSTestActor(wfm: WorkFlowManager) extends Actor {
//  def receive = {
//    case replicatedRequest: GenericMapRequest =>
//      wfm.callWorkFlow(FlowManagerTestData.ivrClientWF.module, replicatedRequest.fields, sender())
//    case _ => println("VCVSTestActor: Unsupported request type")
//  }
//}
//
//
//object FlowManagerTestData {
//
//  val ivrGraphJson: String =
//    """{
//      |	"id": "wf1",
//      |	"name": "IVR",
//      |	"module": "ivr_module",
//      |	"datetime": "date",
//      |	"startNodeId": "gateway123",
//      |	"op": "add",
//      |	"flowchart": {
//      |		"nodes": [{
//      |				"nodeId": "gateway123",
//      |				"name": "name",
//      |				"nType": "gateway",
//      |				"actions": [{
//      |					"formula": "(dnis:=:2008)~||~(dnis:=:2009)",
//      |					"label": "g1"
//      |				}],
//      |				"kafkaTopics": null
//      |			}, {
//      |				"nodeId": "etni123",
//      |				"name": "name",
//      |				"nType": "etni",
//      |				"actions": [{
//      |					"formula": "(omdnStatus:=:ai)",
//      |					"label": "e1"
//      |				}],
//      |				"kafkaTopics": null
//      |			}, {
//      |				"nodeId": "vcvs123",
//      |				"name": "name",
//      |				"nType": "vcvs",
//      |				"actions": [{
//      |						"formula": "(omdnStatus:=:ai)~||~(result:=:yes)~||~(result:=:unknown)",
//      |						"label": "v1"
//      |					},
//      |					{
//      |						"formula": "(omdnStatus:=:ai)~||~(result:=:yes)~||~(result:=:unknown)",
//      |						"label": "v2"
//      |					}
//      |				],
//      |				"kafkaTopics": null
//      |			}, {
//      |				"nodeId": "kafka123",
//      |				"name": "name",
//      |				"nType": "kafka",
//      |				"actions": [],
//      |				"kafkaTopics": "test_topic"
//      |			},
//      |			{
//      |				"nodeId": "session123",
//      |				"name": "name",
//      |				"nType": "session",
//      |				"actions": [],
//      |				"kafkaTopics": null
//      |			}
//      |		],
//      |		"connections": [{
//      |				"connectionId": "c1",
//      |				"pageSourceId": "gateway123",
//      |				"pageTargetId": "etni123",
//      |				"label": "g1"
//      |			},
//      |			{
//      |				"connectionId": "c2",
//      |				"pageSourceId": "etni123",
//      |				"pageTargetId": "vcvs123",
//      |				"label": "e1"
//      |			}, {
//      |				"connectionId": "c3",
//      |				"pageSourceId": "vcvs123",
//      |				"pageTargetId": "session123",
//      |				"label": "v1"
//      |			},
//      |			{
//      |				"connectionId": "c4",
//      |				"pageSourceId": "vcvs123",
//      |				"pageTargetId": "kafka123",
//      |				"label": "v1"
//      |			}
//      |		]
//      |	}
//      |}""".stripMargin
//
//  val ivrClientWF: ClientWFPaylaod = ObjectMapperUtil.fromJson[ClientWFPaylaod](ivrGraphJson)
//
//  //  val cn1 = ClientNode("id1", "name", "etni", List(LabelFormula("(omdnStatus:=:ai)", "e1")))
//  //  val cn2 = ClientNode("id2", "name", "gateway", List(LabelFormula("(dnis:=:2008)~||~(dnis:=:2009)", "g1")))
//  //  val cn3 = ClientNode("id3", "name", "vcvs", List())
//  //
//  //  val ce1 = ClientEdge("c1", "etni", "vcvs", "e1")
//  //  val ce2 = ClientEdge("c2", "gateway", "etni", "g1")
//  //
//  //  val wf = ClientWorkflow("wf1", "IVR", "ivr_module", "date", "add", ClientGraph(List(cn1, cn2, cn3), List(ce1, ce2)))
//  //
//  println("************* " + ObjectMapperUtil.toJson(ivrClientWF))
//
//}