package com.vz.security.sso.fixtures

import akka.actor.ActorRef
import akka.serialization.SerializationExtension

case class TestCaseClass(
  text: String,
  number: Int,
  longNumber: Long,
  anything: Any,
  byteArray: Array[Byte]
  ) {

  import java.io.{ByteArrayOutputStream, ObjectOutputStream}

  def serialise: Array[Byte] = {
    val stream: ByteArrayOutputStream = new ByteArrayOutputStream()
    val oos = new ObjectOutputStream(stream)
    oos.writeObject(this)
    oos.close()
    stream.toByteArray
  }
}