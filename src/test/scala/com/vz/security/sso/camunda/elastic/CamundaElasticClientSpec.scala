package com.vz.security.sso.camunda.elastic

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.sksamuel.elastic4s.embedded.LocalNode
import com.vz.security.sso.objects.CamundaRule
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class CamundaElasticClientSpec extends TestKit(ActorSystem("SecurityAnalyticsCluster"))
  with Matchers with WordSpecLike with ScalaFutures  with BeforeAndAfterAll {

  implicit val defaultPatience = PatienceConfig(timeout = Span(60, Seconds), interval = Span(500, Millis))

  override def beforeAll(): Unit = {
    import com.sksamuel.elastic4s.http.ElasticDsl._
    val localNode = LocalNode("sfgdclusterelastic", "/tmp/datapath")
    val client = localNode.client(shutdownNodeOnClose = true)
  }

  val generatedUUID = UUID.randomUUID()
  "The Camunda Redis Client" should {

    "return true when new rulebook is added" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), uuid = UUID.randomUUID(), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "Test2", module = "test2", desc = "lets test it", byUserId = "v182430", operation="add")
      whenReady(CamundaElasticClient.addRulebook(Option(rule))) {
        value => value shouldBe true
      }
    }

    "return true when retrieving all rulebook ids in system" in {
      whenReady(CamundaElasticClient.getAllRulebooks) {
        //value => (value.isEmpty) shouldBe true
        value => (value.size > 0) shouldBe true
      }
    }


    "return true when new rulebook version is added" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), UUID.fromString("059bd2c3-e85b-4051-9734-a2c602f8bf2e"), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "IG Rule", module = "ig", desc = "lets test it", byUserId = "v182430", operation="add")
      CamundaElasticClient.addRulebook(Option(rule))
      val newVersion = rule.copy(dmnUUID = UUID.randomUUID(), dmnXML = "new xml")
      whenReady(CamundaElasticClient.addRulebookVersion(Option(newVersion))) {
        value => value shouldBe true
      }
    }

    "return list of rule versions when specific rulebook id is provided" in {
      whenReady(CamundaElasticClient.getRulebookVersions(UUID.fromString("059bd2c3-e85b-4051-9734-a2c602f8bf2e") )){
        ruleVersionList => (ruleVersionList.length > 0) shouldBe true
      }
    }

    "return true when rules with dmn is returned" in {
      whenReady(CamundaElasticClient.getAllRulebooksWithDMN){
        ruleDmnList => (ruleDmnList.length > 0) shouldBe true
      }
    }

    "return true when multiple dmn files are deleted" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), UUID.fromString("059bd2c3-e85b-4051-9734-a2c602f8bf2e"), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "IG Rule", module = "ig", desc = "lets test it", byUserId = "v182430", operation="add")
      whenReady(CamundaElasticClient.deleteRulebookDmns(Option(List(UUID.fromString("a7f970f1-5214-42de-9d59-f6ec77af3093"), UUID.fromString("6e965ec0-fa2c-4af2-8192-d8af32b7f915"), UUID.fromString("1c185579-f9be-4a78-9f56-67319358713b"), UUID.fromString("786503e9-95ec-48c9-9348-5006d6bb2813"))))) {
        value => value shouldBe true
      }
    }

  }
}
