package com.vz.security.sso.grpc.interceptors

import java.util.concurrent.TimeUnit

import io.grpc.{ClientCall, ForwardingClientCallListener, Metadata, Status}


class ClientPerformanceCallListner[S](val delegate1: ClientCall.Listener[S], val startTime: Long) extends ForwardingClientCallListener[S] {

  override def delegate(): ClientCall.Listener[S] = delegate1

  override def onMessage(message: S): Unit = {
    super.onMessage(message)
  }

  override def onClose(status: Status, trailers: Metadata): Unit = {
    val durationInMicro = TimeUnit.NANOSECONDS.toMicros(System.nanoTime - startTime)
//    println(s"status - ${status.getCode.name} >> elapsed time:: ${durationInMicro} µs")//println("closing connection")
    super.onClose(status, trailers)
  }

}
