package com.vz.security.sso.encryption


import com.vz.security.sso.encryption.EncryptionConfig._

import scala.collection.mutable

object EncryptionUtil {

  private val ENCRYPT_MAP = new mutable.HashMap[EncryptInfo, Encrypt]()

  /**
   * Returns Encrypt instance if exist
   * or create new instance and return
   */
  private def getOrCreate(info: EncryptInfo): Encrypt = {
    if (ENCRYPT_MAP.get(info).isDefined) ENCRYPT_MAP(info)
    else {
      //Only one instance of Encrypt should be created for each EncryptInfo
      ENCRYPT_MAP.synchronized {
        if (ENCRYPT_MAP.get(info).isDefined) ENCRYPT_MAP.get(info)
        else {
          ENCRYPT_MAP += (info -> AES256Encrypt(_encryptionKey = info.encryptionKey, _macEncryptionKey = info.macEncryptionKey, _iterationCount = info.iterationCount))
        }
        ENCRYPT_MAP(info)
      }
    }
  }

  /**
   *
   * @param info      Details of encryption type
   * @param clearText Text to encrypt
   * @return Encrypted text
   */
  def encrypt(info: EncryptInfo)(clearText: String): String = {
    getOrCreate(info).encrypt(clearText)
  }

  /**
   *
   * @param info Details of encryption type
   * @param map  Map to encrypt
   * @return Encrypted text
   */
  def encryptMap(info: EncryptInfo)(map: Map[String, String]): String = {
    val clearText = mapToString(map)
    getOrCreate(info).encrypt(clearText)
  }

  /**
   *
   * @param info          Details of encryption type
   * @param encryptedText Text to decrypt
   * @return Decrypted text
   */
  def decrypt(info: EncryptInfo)(encryptedText: String): String = {
    getOrCreate(info).decrypt(encryptedText)
  }

  /**
   *
   * @param info          Details of encryption type
   * @param encryptedData Map in text format to decrypt
   * @return Decrypted text
   */
  def decryptMap(info: EncryptInfo)(encryptedData: String): Map[String, String] = {
    val encText = getOrCreate(info).decrypt(encryptedData)
    stringToMap(encText)
  }

  /**
   *
   * @param inMap Input Map
   * @return String by merging key values using delimiters
   */
  def mapToString(inMap: Map[String, String]): String = {
    inMap.map(_.productIterator.mkString(KVDELIMITER))
      .mkString(PAIRDELIMITER)
  }

  /**
   *
   * @param inText Input text
   * @return Map by splitting string into key values using delimiters
   */
  def stringToMap(inText: String): Map[String, String] = {
    inText.split(PAIRDELIMITER).map(m => m.split(KVDELIMITER)).map(kv => kv(0) -> kv(1)).toMap
  }

}
