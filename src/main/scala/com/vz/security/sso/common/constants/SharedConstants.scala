package com.vz.security.sso.common.constants

object SharedConstants {

  // to be used across all the Apps
  val Actor_System_Name: String = "SecurityAnalyticsCluster"

  // to be used in GetInitialRiskScore class
  val BlackList_Increment_Key: String = "BlackList"
  val WhiteList_Increment_Key: String = "WhiteList"
  val CountryCode_US: String = "US"
  val US_Hit: String = "US-Hit"
  val NonUS_Hit: String = "NonUS-Hit"
  val CountryNotFound: String = "CountryNotFound"

  // to be used in Gateway App
  val IG_HIT: String = "IG-HIT"
  val IG_NO_ARGUMENTS = "IG-NO-ARGUMENTS"
  val SSO_HIT = "SSO-HIT"
  val SSO_NO_ARGUMENTS = "SSO-NO-ARGUMENTS"
  val CLEAR_AND_RELOAD_HIT = "CLEAR_AND_RELOAD_HIT"

  //CacheManager constants

  val RANGE_CACHE = "RangeCache"
  val IP_WHITELIST_CACHE = "IPWhiteListCache"
  val IP_COUNTRY_RANGE_CACHE = "IPCountryRangeCache"
  val LFU_IP_CACHE = "LFUIPCache"
  val LFU_USER_CACHE = "LFUUserCache"

  // stream App
  val now = "now"

  /**
    * Circuit Breaker - GRPC
    * These dummy messages are used to test GRPC connection in half opened Circuit Breaker scenario
    */
  val halfOpenMessage = Map[String, String]("halfOpened" -> "Testing server connection")
  val halfOpenMessageType = "ConnectionTest"
  val grpcTimestamp = "grpcTimestamp"
  val unsupportedReplicationType = "unsupportedReplicationType"

  /**
    * Cache manager constants
    * Elastic search index and types.
    */
  val cacheConfigIndex = "managed_caches"
  val cacheConfigType = "_doc"
  val mangoIndex = "hashed_cache_data"
  val mangoType = "_doc"
  val rangeIndex = "range_cache_snapshots"
  val rangeType = "_doc"

  //Cache operations
  val ADD = "add"
  val REMOVE = "remove"
  val UPDATE = "update"

  //IG Persistence
  val SHARDKEY = "shardKey"
  val SHARDVALUE = "shardValue"
  val ACTORTHRESHOLD = "actorThreshold"
  val MAXTHRESHOLD = "100"
  val ACTORSTOCALL = "actorsToCall"
  val CHAINDELIMITER = ","
  val SCOREDELIMITER = ":"
  val RISKSCORE = "riskScore"
  val REASONFORDECISION = "reasonForDecision"
  val NOREASON = "NoReasonFound"
  val defaultRiskScore = "50"
  val defaultRiskScoreMessage = "default"
  val USERID = "userID"

  // to be used in StaticApp & DyanmicApp
  val HTTPROUTESSO: String = "sso"
  val HTTPROUTEOAUTH: String = "oauth"
  val HTTPROUTE: String = "httpRoute"
  val HTTPROUTEPINDATA: String = "pindata"

  val WFNODEID = "wfNodeId"
  val NTYPE = "nodeType"
  val WFID = "wfId"
  val ENRICHKEYS = "enrichKeys"
  val COPYKEYS = "copyKeys"
  val FILTERKEYS = "filterKeys"
  val KEYSDELIMITER = ";"
  val VALUEDELIMITER = ":"
  val SCORE = "score"


  val ACTORID = "actorId"
  val GENERICSHARD = "genericShard"
  val RTSHARD = "rtShard"
  val REPLY = "reply"

  // CCPA constants

  val reqType = "requestType"
  val guestLogin = "GuestLogin"
  val sendOTP = "SendOTP"
  val validateOTP = "ValidateOTP"
  val profileUpdate = "ProfileUpdate"
  val wfmDesicion = "WFMDecision"
  val fileReady = "CXPDownoadReady"
  val sendFileFormat = "sendFileFormat"
  val getProfile = "GetProfile"
  val getToken = "GetToken"


  // Attributes

  val mdn = "mdn"
  val email = "email"
  val ipaddress = "ipaddress"
  val fname = "fname"
  val lname = "lname"
  val address1 = "address1"
  val address2 = "address2"
  val city = "city"
  val zip = "zip"
  val state = "state"
  val zipcode = "zipcode"
  val consent = "consent"
  val uuid = "uuid"
  val status = "status"
  val otp = "otp"
  val guestOTP = "GuestOTP"
  val action = "action"
  val otpValidationResult = "OTPValidationResult"
  val amResultProfileUpdate = "amResultProfileUpdate"
  val ProfileUpdateFailureResponse = "ProfileUpdateFailureResponse"
  val success = "success"
  val failed = "failed"
  val successStatusCode = "200"
  val failureStatusCode = "404"
  val ServerErrorStatusCode = "500"
  val reason = "reason"
  val url = "url"
  val token = "token"
  val gemaltoLink = "gemaltoLink"
  val fileFormat = "fileFormat"

  // Empty strings
  val emptyString = ""
  val _emptyString = "_"

  //Actor states

  val initialState = "initial state"
  val previousState = "PreviousState"
  val presentState = "PresentState"

  val APP_NAME_AM= "AM"
  val APP_NAME_ENFORCES = "ENFORCE"
  val APP_NAME_CXP= "CXP"

  val serverErrormsg = "internal server error"

  //CXP Constants
  val mtn = "mtn"
  val emailID = "emailId"
  val firstName = "firstName"
  val lastName = "lastName"
  val address = "address"
  val accountId = "accountId"
  val categoryArrVal = "all"
  val downloadType = "REGULAR"
  val formatType = "zip"
  val requestChannel = "IG"
  val role = "guest"
  val serviceType = "guest"
  val download = "download"
  val delete = "delete"
  val apikey = "X-apikey"
  val correlationId = "correlationId"
  val ipAddr = ""
  val optFlag = "Y"
  val cxperrorMessage = "errorMessage"
  //Enforce
  val enforceProfileUpdate = "EnforceProfileUpdate"
  val serviceName = "documentRequest"
  val clientAppName = "Verizon CCPA Team"
  val documentType1 ="2"
  val documentType2 ="3"
  val documentType3 ="5"
  val applicationDocumentType = "guest-flow"
  val applicationDocumentId = "applicationDocumentId"
  val addr1 = "addr1"
  val addr2 = "addr2"
  val stateCd = "stateCd"
  val customerName = "customerName"
  val enfoEroorMSG = "message"
  val cxpError = "cxp not responding"

  //idm service states
  val guestLoginStatus = "guestLoginStatus"
  val sendOTPStatus = "sendOTPStatus"
  val validateOTPStatus = "validateOTPStatus"
  val profileUpdateState = "profileUpdateState"
  val getProfileState = "getProfileState"
  val fileReadyState = "fileReadyState"
  val wfmDesicionState = "wfmDesicionState"
  val enforceProfileUpdateStatus = "enforceProfileUpdateStatus"

}
