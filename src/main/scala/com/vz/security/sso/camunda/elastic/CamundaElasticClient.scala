package com.vz.security.sso.camunda.elastic

import java.util.UUID

import akka.actor.ActorSystem
import com.sksamuel.elastic4s.http.delete.DeleteResponse
import com.sksamuel.elastic4s.http.index.IndexResponse
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.objects.CamundaRule
import com.sksamuel.elastic4s.RefreshPolicy
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties, Response}
import com.sksamuel.elastic4s.http.search.{SearchHit, SearchResponse}
import com.sksamuel.elastic4s.http.update.UpdateResponse
import com.vz.security.sso.logging.{VZLogger, VZQuickLogger}

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.generic.extras.Configuration
import io.circe.parser._
import io.circe.{Decoder, HCursor, Json}
import util.control.Breaks._

import scala.util.Success

object CamundaElasticClient {
  implicit val customCirceConfig: Configuration = Configuration.default.withDefaults

  import scala.concurrent.ExecutionContext.Implicits._
  import com.sksamuel.elastic4s.http.ElasticDsl._


  val config: Config = ConfigFactory.load()
  val esClusterConf: String = config.getString("sso.camunda.elastic.url")
  val esclient = ElasticClient(ElasticProperties(esClusterConf))

  val logger = new VZQuickLogger(this)

  def addRulebook(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.byUserId == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    logger.info("addRuleBook", null, "ruleTimestamp", rule.get.timestamp, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)
    val addRule =  for {
      result <- addRulebookVersion(rule)
    } yield (result)
    addRule
  }

  def getAllRulebooks: Future[List[CamundaRule]] = {
    val fromElastic = for {
      ruleBookIdSet <- getRulebookConfigUUIDs
      ruleBookSet: Set[Option[CamundaRule]] <- Future.sequence(ruleBookIdSet map {
        id => getLatestRulebookVersionWithoutDMN(id)
      })
    } yield ruleBookSet.toList.flatten
    fromElastic
  }

  def getAllRulebooksWithDMN: Future[List[CamundaRule]] = {
    val fromElastic = for {
      ruleBookIdSet <- getRulebookConfigUUIDs
      ruleBookSet: Set[Option[CamundaRule]] <- Future.sequence( ruleBookIdSet map {
        id => getRuleByLatestVersionWithDMN(id)
      })
    } yield ruleBookSet.toList.flatten
    fromElastic

  }

  def getRulebookConfigUUIDs: Future[Set[UUID]] = {

    val futureResult: Future[Response[SearchResponse]] = esclient.execute {
      search("rulebookversion").size(2000)
    }

    val esResult = futureResult.map { listres =>
      val uuidSet =  scala.collection.mutable.SortedSet[UUID]()
      val hitsQ = listres.result.hits.hits

      hitsQ.foreach { allhitsParent =>
        val rulejson: Json = parse(allhitsParent.sourceAsString).getOrElse(Json.Null)
        val cursor : HCursor = rulejson.hcursor
        val _uuid = cursor.downField("uuid").focus.get
        val uuidEntry = UUID.fromString(_uuid.toString.replaceAll("\"", ""))
        if (!uuidSet.contains(uuidEntry)) {
          uuidSet += uuidEntry
        }
      }
      uuidSet.toSet
    }
    esResult
  }

  def deleteRulebook(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.byUserId == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    logger.info("deleteRulebook", null, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)
    val res = for {
      versionsDeleted <- deleteAllRulebookVersions(rule)
    } yield versionsDeleted

    res
  }

  def deleteAllRulebookVersions(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    logger.info("deleteAllRulebookVersions", null, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)
    for {
      versionList <- getRulebookVersions(rule.get.uuid)
      dmnFilesDeleted <- deleteRulebookDmns(Option(versionList.map(_.dmnUUID)))
      versionToDelete <- getRuleBookVersionsForDelete(rule)
      deleteVersions <- handleDeleteVersions(versionToDelete)
    } yield deleteVersions

  }

  def getRuleBookVersionsForDelete(rule: Option[CamundaRule]): Future[Response[SearchResponse]] = {
    val futureResult: Future[Response[SearchResponse]] = esclient.execute {
      search("rulebookversion").matchQuery("uuid", rule.get.uuid.toString).sortByFieldDesc("timestamp")
    }
    futureResult
  }

  def handleDeleteVersions(vers: Response[SearchResponse]) : Future[Boolean] = {
    val hitsQ = vers.result.hits.hits
    val verslist = hitsQ.map(v =>
      deleteVersionToElastic(v.id.toString)
    )
    Future.foldLeft(verslist.toList)(false) {case(finalRes, res) =>
      finalRes || res.isSuccess
    }
  }

  def deleteRulebookDmns(dmnUUIDList: Option[List[UUID]]): Future[Boolean] = {
    val uuidStrList = dmnUUIDList.getOrElse(List()).map( _.toString )
      val uuidresult = uuidStrList.map(ddmn =>
        deleteDmnToElastic(ddmn)
      )
      Future.foldLeft(uuidresult)(false) {case(finalRes, res) =>
        finalRes || res.isSuccess
      }
  }



  def addRulebookVersion(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.dmnUUID == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }

    logger.info("addRuleBookVersion", null,
      "ruleTimestamp", rule.get.timestamp,
      "ruleUUID", rule.get.uuid.toString,
      "ruleDmnUUID", rule.get.dmnUUID.toString,
      "byUserId", rule.get.byUserId)

    val ruleJson = rule.get.copy(dmnXML = "").asJson.noSpaces //dmmn will be stored in different store
    val res = for {
      isAdded <- indexRulebookVersion(ruleJson)
      dmnXmlUploaded <- addRulebookDmn(rule)
    } yield dmnXmlUploaded
    res
  }

  def indexRulebookVersion(ruleJson: String): Future[Response[IndexResponse]] = {
    val childResult: Future[Response[IndexResponse]] = esclient.execute {
      indexInto("rulebookversion" / "version").doc(ruleJson).refresh(RefreshPolicy.Immediate)
    }
    childResult
  }

  def deleteDmnToElastic(uuid: String): Future[Response[DeleteResponse]] = {
    val futureResult: Future[Response[DeleteResponse]]  = esclient.execute {
      delete(uuid).from("rulebookdmn" / "dmn").refresh (RefreshPolicy.Immediate)
    }
    futureResult
  }

  def deleteVersionToElastic(vuuid: String): Future[Response[DeleteResponse]] = {
    val futureResult: Future[Response[DeleteResponse]] = esclient.execute {
      delete(vuuid).from("rulebookversion" / "version").refresh (RefreshPolicy.Immediate)
    }
    futureResult
  }
  def addRulebookDmn(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.dmnUUID == null) || (rule.get.dmnXML == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }

    logger.info("addRulebookDmn", null,
      "ruleTimestamp", rule.get.timestamp,
      "ruleUUID", rule.get.uuid.toString,
      "ruleDmnUUID", rule.get.dmnUUID.toString,
      "byUserId", rule.get.byUserId)

    val dmn = rule.get.dmnXML
    val dmnuuid = rule.get.dmnUUID
    val dmnEntry = DmnEntry(dmnuuid.toString, dmn)

    val childResult: Future[Response[UpdateResponse]] = esclient.execute {
      update(dmnuuid.toString).in("rulebookdmn/dmn").docAsUpsert(dmnEntry.asJson.toString()).refresh (RefreshPolicy.Immediate)
    }
    childResult.map {
      res => res.isSuccess
    }
  }

  def getRuleDmn(dmnUUID: Option[UUID]): Future[Option[String]] = {
    val str: Option[String] = Option("str")

    val uuidStr = dmnUUID.getOrElse(throw new NullPointerException("UUID is not passed")).toString
    val futureResult: Future[Response[SearchResponse]] = esclient.execute {
      search("rulebookdmn").matchQuery("dmnUUID", uuidStr)
    }

    val dmnXML = futureResult.map { esres =>
      val hitsQ = esres.result.hits.hits
      val option = new StringBuilder
      val list: String =  {
        breakable {
          hitsQ.foreach { v =>
            if (v.id == uuidStr) {
              val dmnjson: Json = parse(v.sourceAsString).getOrElse(Json.Null)
              val cursor: HCursor = dmnjson.hcursor
              val dmnXML = cursor.downField("dmnXML").focus.get
              option.append(dmnXML.toString)
              break
            }
          }
        }
        option.toString
      }
      Option(list)
    }
    dmnXML
  }

  def getRuleByLatestVersionWithDMN(ruleUUID: UUID): Future[Option[CamundaRule]] = {
    for {
      latestRule <- getLatestRulebookVersionWithoutDMN(ruleUUID)
      dmnXMLOpt <- {
        if(latestRule.isDefined) getRuleDmn(Option(latestRule.get.dmnUUID)) else Future.successful(None)
      }
      finalRuleWithDMN <- Future.successful(Option(latestRule.get.copy(dmnXML = dmnXMLOpt.getOrElse(""))))
    } yield finalRuleWithDMN

  }

  def getLatestRulebookVersionWithoutDMN(ruleUUID: UUID): Future[Option[CamundaRule]] = {
    for {
      listOfRules: Seq[CamundaRule] <- getRulebookVersions(ruleUUID)
      latestRule: Option[CamundaRule] <- if (listOfRules.nonEmpty) Future.successful(Option(listOfRules.head)) else Future.successful(None)
    } yield latestRule

  }

  def getRulebookVersions(ruleUUID: UUID): Future[List[CamundaRule]] = {
    if (ruleUUID == null) {
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }

    logger.info("getRulebookVersions", null,"ruleUUID", ruleUUID)
    val futureResult: Future[Response[SearchResponse]] = esclient.execute {
      search("rulebookversion").matchQuery("uuid", ruleUUID.toString).sortByFieldDesc("timestamp")
    }
    val esResult = futureResult.map { esres =>
      val hitsQ = esres.result.hits.hits
      val camundalist = new ListBuffer[CamundaRule]()
      hitsQ.foreach { v =>
        decode[CamundaRule](v.sourceAsString) match {
          case Left(ex: Exception) => logger.error("getRulebookVersions", null, ex)
          case Right(rule) =>
            logger.debug("getRulebookVersions", null, "dmnUUID", rule.dmnUUID)
            camundalist += rule
        }
      }
      camundalist.toList
    }
    esResult
  }
}

case class DmnEntry(dmnUUID: String, dmnXML: String)

