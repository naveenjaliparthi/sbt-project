package com.vz.security.sso.camunda.redis

import java.util.UUID

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import com.vz.security.sso.logging.{VZLogger, VZQuickLogger}
import com.vz.security.sso.objects.CamundaRule
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.generic.extras.Configuration
import scredis.util.LinkedHashSet
import scredis.{RedisCluster, Score}
import scredis._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContextExecutor, Future }

object CamundaRedisClient {

  implicit val customCirceConfig: Configuration = Configuration.default.withDefaults
  import scala.concurrent.ExecutionContext.Implicits._

  private val RedisCamundaRulesVersionKey = "camunda:rules:versions:ruleUUID"
  private val RedisCamundaRulesDmnKey = "camunda:rules:dmn"
  private val RedisCamundaRuleBookSetKey = "camunda:rulebooks"

  private var redisClient: RedisCluster = _
  private var providedActorSystem: Option[ActorSystem] = None
  private val conf = ConfigFactory.load()
  private val camundaRedisConfig = conf.getConfig("sso.camunda.redis")

  private val maxVersions = camundaRedisConfig.getInt("max-versions")


  def initialize(actorSystem: ActorSystem): Unit = {
    val logger = new VZLogger(this, "initialize")
    providedActorSystem = Some(actorSystem)

    try {
      val scredisConfig = conf.getConfig("scredis.redis")
      logger.addKeyValue("redis_cluster_nodes", scredisConfig.getStringList("cluster-nodes"))
      logger.info

      redisClient = RedisCluster(providedActorSystem = providedActorSystem)
      if(redisClient == null) throw new NullPointerException
    } catch {
      case ex : Exception => logger.error(ex)
    }
  }

  def addRulebook(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.byUserId == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("addRuleBook", null, "ruleTimestamp", rule.get.timestamp, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)

    // Import internal ActorSystem's dispatcher (execution context) to register callbacks
    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      noOfEntriesInserted <- {
        //first make entry to rulebook set
        redisClient.sAdd(RedisCamundaRuleBookSetKey ,  rule.get.uuid.toString)
      }
      newVersionAdded <- {
        logger.info("addRuleBook", null,"ruleUUID", rule.get.uuid, "result", noOfEntriesInserted > 0 )
        if(noOfEntriesInserted > 0) addRulebookVersion(rule)
        else Future.successful(false)
      }
    } yield newVersionAdded

    res
  }

  def getAllRulebooks: Future[List[CamundaRule]] = {
    val logger = new VZQuickLogger(this)
    logger.info("getAllRulebooks", null)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      ruleBookIdSet <- redisClient.sMembers(RedisCamundaRuleBookSetKey)
      ruleBookSet: Set[Option[CamundaRule]] <- Future.sequence( ruleBookIdSet map {
        id => getLatestRulebookVersionWithoutDMN(UUID.fromString(id), 0 , 0)
      })
    } yield ruleBookSet.toList.flatten
    res
  }

  def getAllRulebooksWithDMN: Future[List[CamundaRule]] = {
    val logger = new VZQuickLogger(this)
    logger.info("getAllRulebooks", null)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      ruleBookIdSet <- redisClient.sMembers(RedisCamundaRuleBookSetKey)
      ruleBookSet: Set[Option[CamundaRule]] <- Future.sequence( ruleBookIdSet map {
        id => getRuleByLatestVersionWithDMN(UUID.fromString(id))
      })
    } yield ruleBookSet.toList.flatten

    res
  }

  def deleteRulebook(rule: Option[CamundaRule]) :  Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.byUserId == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("deleteRulebook", null, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)

    // Import internal ActorSystem's dispatcher (execution context) to register callbacks
    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      versionsDeleted <- deleteAllRulebookVersions(rule)
      noOfEntriesRemoved <- redisClient.sRem(RedisCamundaRuleBookSetKey ,  rule.get.uuid.toString)
      allDeleted <- Future.successful(versionsDeleted && (noOfEntriesRemoved > 0))
    } yield allDeleted

    res
  }

  /*
  //first retrieve all versions to get DMN UUIDs
  //delete dmn files
  //delete rulebook version sorted set
  */
  def deleteAllRulebookVersions(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("deleteAllRulebookVersions", null, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    for {
      versionList <- getRulebookVersions(rule.get.uuid, 0, -1)
      dmnFilesDeleted <- deleteRulebookDmns(Option(versionList.map(_.dmnUUID)))
      noOfKeysDeleted <- redisClient.del(RedisCamundaRulesVersionKey.replace("ruleUUID", rule.get.uuid.toString))
      isEverythingDeleted <- Future.successful(dmnFilesDeleted && (noOfKeysDeleted > 0))
    } yield isEverythingDeleted
  }

  def maintainMaxRulebookVersions(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("maintainMaxRulebookVersions", null, "ruleUUID", rule.get.uuid.toString, "byUserId", rule.get.byUserId)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    for {
      versionList <- getRulebookVersions(rule.get.uuid, maxVersions, -1)
      dmnFilesDeleted <- deleteRulebookDmns(Option(versionList.map(_.dmnUUID)))
      noOfKeysDeleted <- {
        val maxScore: Double = if(versionList.nonEmpty ) {
          versionList.head.timestamp.toDouble
        } else Double.box(0)

        redisClient.zRemRangeByScore(
          RedisCamundaRulesVersionKey.replace("ruleUUID", rule.get.uuid.toString),
          ScoreLimit.MinusInfinity,
          scredis.ScoreLimit.Inclusive(maxScore)
        )
      }
      isEverythingDeleted <- Future.successful(dmnFilesDeleted && (noOfKeysDeleted > 0))
    } yield isEverythingDeleted
  }


  def deleteRulebookDmns(dmnUUIDList: Option[List[UUID]]): Future[Boolean] = {
    //delete all dmns passed
    val uuidStrList = dmnUUIDList.getOrElse(List()).map( _.toString )

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    for {
      noOfFilesDeleted <- {
        if(uuidStrList.nonEmpty) redisClient.hDel(RedisCamundaRulesDmnKey, uuidStrList: _*)
        else Future.successful(0L)
      }
      isAllDeleted <- Future.successful(noOfFilesDeleted > 0)
    } yield isAllDeleted
  }

  def addRulebookVersion(rule: Option[CamundaRule]) :  Future[Boolean] = {
    if(rule.isEmpty || (rule.get.uuid == null) || (rule.get.dmnUUID == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("addRuleBookVersion", null,
                  "ruleTimestamp", rule.get.timestamp,
                          "ruleUUID", rule.get.uuid.toString,
                          "ruleDmnUUID", rule.get.dmnUUID.toString,
                          "byUserId", rule.get.byUserId)

    // Import internal ActorSystem's dispatcher (execution context) to register callbacks
    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val ruleJson = rule.get.copy(dmnXML = "").asJson.noSpaces  //dmmn will be stored in different store

    val res = for {
      isAdded <- redisClient.zAdd(RedisCamundaRulesVersionKey.replace("ruleUUID", rule.get.uuid.toString) , ruleJson, Score(rule.get.timestamp.toString))
      dmnXmlUploaded <- {
        logger.info("addRulebookVersion", null,"ruleDmnUUID", rule.get.uuid, "result", isAdded)
        if(isAdded) addRulebookDmn(rule)
        else Future.successful(false)
      }
      _ <- maintainMaxRulebookVersions(rule)
    } yield dmnXmlUploaded

    res
  }


  def addRulebookDmn(rule: Option[CamundaRule]): Future[Boolean] ={
    if(rule.isEmpty || (rule.get.dmnUUID == null) || (rule.get.dmnXML == null)){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("addRulebookDmn", null,
                  "ruleTimestamp", rule.get.timestamp,
                          "ruleUUID", rule.get.uuid.toString,
                          "ruleDmnUUID", rule.get.dmnUUID.toString,
                          "byUserId", rule.get.byUserId)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      isUploaded <- redisClient.hSet(RedisCamundaRulesDmnKey, rule.get.dmnUUID.toString, rule.get.dmnXML)
    } yield {
      logger.info("addRulebookDmn", null,"ruleDmnUUID", rule.get.dmnUUID.toString, "result", isUploaded)
      isUploaded
    }

    res
  }

  def getRuleDmn(dmnUUID: Option[UUID]): Future[Option[String]] = {

    val uuidStr = dmnUUID.getOrElse(throw new NullPointerException("UUID is not passed")).toString
    val logger = new VZLogger(this, "getRuleDmn")
    logger.addKeyValue("ruleUUID", uuidStr)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    for{
      dmlXML <- redisClient.hGet(RedisCamundaRulesDmnKey, uuidStr)
    } yield dmlXML
  }

  def getRuleByLatestVersionWithDMN(ruleUUID: UUID): Future[Option[CamundaRule]] = {

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher

    val logger = new VZLogger(this, "getRuleByLatestVersion")
    logger.addKeyValue("ruleUUID", ruleUUID)

    for {
      latestRule <- getLatestRulebookVersionWithoutDMN(ruleUUID, 0, 0)
      dmnXMLOpt <- {
        if(latestRule.isDefined) getRuleDmn(Option(latestRule.get.dmnUUID)) else Future.successful(None)
      }
      finalRuleWithDMN <- Future.successful(Option(latestRule.get.copy(dmnXML = dmnXMLOpt.getOrElse(""))))
    } yield finalRuleWithDMN
  }

  def getLatestRulebookVersionWithoutDMN(ruleUUID: UUID, start: Int, stop: Int): Future[Option[CamundaRule]] = {
    for {
      listOfRules: Seq[CamundaRule] <- getRulebookVersions(ruleUUID, start, stop)
      latestRule: Option[CamundaRule] <- if (listOfRules.nonEmpty) Future.successful(Option(listOfRules.head)) else Future.successful(None)
    } yield latestRule
  }

  def getRulebookVersions(ruleUUID: UUID, start: Int, stop: Int): Future[List[CamundaRule]] = {
    if(ruleUUID == null){
      Future.failed(new IllegalArgumentException("Missing one of the parameter value"))
    }
    val logger = new VZQuickLogger(this)
    logger.info("getRulebookVersions", null,"ruleUUID", ruleUUID, "start", start, "stop", stop)

    implicit  val dispatcher: ExecutionContextExecutor = providedActorSystem.getOrElse(throw new NullPointerException("ActorSystem is null")).dispatcher
    val res = for {
      versions: LinkedHashSet[String] <- redisClient.zRevRange(RedisCamundaRulesVersionKey.replace("ruleUUID", ruleUUID.toString), start, stop )
      ruleBookVersions <- {
        val camundalist = new ListBuffer[CamundaRule]()
        versions.foreach { v =>
          // camundalist += decoder[CamundaRule](v)
          decode[CamundaRule](v) match {
            case Left(ex: Exception) => logger.error("getRulebookVersions", null, ex)
            case Right(rule) =>
              logger.debug("getRulebookVersions", null, "dmnUUID", rule.dmnUUID)
              camundalist += rule
          }
        }
        Future.successful(camundalist.toList)
      }
    } yield ruleBookVersions
    res
  }

}
