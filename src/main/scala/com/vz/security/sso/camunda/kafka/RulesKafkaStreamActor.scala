package com.vz.security.sso.camunda.kafka

import java.util.UUID
import java.util.UUID.randomUUID

import akka.actor.{Actor, PoisonPill, Props}
import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.pattern.{Backoff, BackoffSupervisor}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, KillSwitches, Supervision}
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.camunda.RuleEngine.{confTargetCluster, processRuleBook}
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.objects.CamundaRule
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import org.apache.kafka.clients.consumer.ConsumerConfig
import com.vz.security.sso.config.KafkaConfigLoader
import com.vz.security.sso.utils.ObjectMapperUtil
import org.apache.kafka.common.serialization.StringDeserializer

import scala.util.{Failure, Success, Try}


case class RulesKafkaConfig(topic: String, offset: String, groupId: String, kakfkaBootStrap: String)

object RulesKafkaConfig {
  def apply(config: Config): RulesKafkaConfig = {
    val offset = if (config.hasPath("offset.setting")) {
      config getString "offset.setting"
    } else "earliest"

    val groupId = if (config.hasPath("groupId")) {
      config getString "groupId"
    } else UUID.randomUUID().toString

    RulesKafkaConfig(
      config getString "topic",
      offset,
      groupId,
      config getString "kakfkaBootStrap"
    )
  }
}

trait RulesKafkaStreamActor extends Actor {

  val _config: RulesKafkaConfig


  override def preStart(): Unit = {
    super.preStart()
    setUpListner()
  }

  def setUpListner(): Unit = {
    implicit val executionContext = context.dispatcher
    implicit val materializer = ActorMaterializer()

    val noSink = Sink.ignore
    val sharedKillSwitch = KillSwitches.shared("my-kill-switch")

    val consumerSettings = KafkaConfigLoader.getConsumerSettings(_config)

    // Consumer Logic
    val done = Consumer.committableSource(consumerSettings, Subscriptions.topics(_config.topic)).map(msg => {
      val logger = new VZLogger(this, "consume_new_rule")
      try {
        val newRule = decode[CamundaRule](msg.record.value)
        newRule match {
          case Left(ex: Exception) => logger.error(ex)
          case Right(rule) =>
            val processRB: Boolean = processRuleBook(rule.inputConfig.get)
            val newRulePublished = Option(rule)
            logger.addKeyValue("uuid", newRulePublished.get.uuid)
            logger.addKeyValue("timestamp", newRulePublished.get.timestamp)
            logger.addKeyValue("dmnUUID", newRulePublished.get.dmnUUID)
            logger.addKeyValue("confTargetCluster", confTargetCluster)
            logger.addKeyValue("processRuleBook", processRB)
            logger.info
            if (processRB) process(newRulePublished)
        }
      } catch {
        case ex: Exception =>
          logger.error(ex)
      }
      msg.committableOffset
    }).batch(max = 2000, first => CommittableOffsetBatch.empty.updated(first)) { (batch, elem) =>
      batch.updated(elem)
    }.mapAsync(3)(_.commitScaladsl())
      .via(sharedKillSwitch.flow)
      .runWith(noSink)

    done.onComplete {
      case Failure(e) =>
        val logger = new VZLogger(this, "RulesKafkaStreamActorSupervisor")
        logger.addKeyValue("stack_trace", e.printStackTrace())
        logger.addKeyValue("error_message", e.getMessage)
        logger.addKeyValue("error_source", "decider supervision strategy in RulesKafkaStreamActorSupervisor from RangeCache")
        logger.error(e)
        self ! PoisonPill
      case Success(_) => self ! PoisonPill
    }
  }

  def process(value: Option[CamundaRule]): Boolean

}
