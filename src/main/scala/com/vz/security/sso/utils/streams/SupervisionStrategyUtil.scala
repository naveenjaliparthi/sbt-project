package com.vz.security.sso.utils.streams

import akka.stream.Supervision
import com.vz.security.sso.common.exceptions.CustomExceptions.{HarmlessException, ObjectMapperException}
import com.vz.security.sso.logging.{VZLogger, VZQuickLogger}
import org.apache.kafka.common.KafkaException

object SupervisionStrategyUtil {

  val logger = new VZQuickLogger(this)

  /**
    * AKKA STREAMS SUPERVISION STRATEGIES
    */
  lazy val systemDecider: Supervision.Decider = {
    case ex: Exception =>
      logMessage("systemDecider", ex)
      Supervision.Resume
  }

  lazy val unmarshallingFlowDecider: Supervision.Decider = {
    case ex: ObjectMapperException =>
      logMessage("unmarshallingFlowDecider", ex)
      Supervision.Resume
    case ex: NullPointerException =>
      logMessage("unmarshallingFlowDecider", ex)
      Supervision.Resume
    case ex: Exception =>
      logMessage("unmarshallingFlowDecider", ex)
      Supervision.Resume
  }

  lazy val flowDecider: Supervision.Decider = {
    case ex: HarmlessException =>
      logger.info("flowDecider", null, "HarmlessException", ex.message)
      Supervision.Resume
    case ex: NullPointerException =>
      logMessage("flowDecider", ex)
      Supervision.Resume
    case ex: Exception =>
      logMessage("flowDecider", ex)
      Supervision.Resume
  }

  lazy val sourceDecider: Supervision.Decider = {
    case ex: KafkaException =>
      logMessage("sourceDecider", ex)
      Supervision.Resume
    case ex: NullPointerException =>
      logMessage("sourceDecider", ex)
      Supervision.Resume
    case ex: Exception =>
      logMessage("sourceDecider", ex)
      Supervision.Resume
  }

  lazy val sinkDecider: Supervision.Decider = {
    case ex: KafkaException =>
      logMessage("sinkDecider", ex)
      Supervision.Resume
    case ex: NullPointerException =>
      logMessage("sinkDecider", ex)
      Supervision.Resume
    case ex: Exception =>
      logMessage("sinkDecider", ex)
      Supervision.Resume
  }

  def logMessage(key: String, ex: Throwable) = {
    logger.error(key, null, ex)
  }
}
