package com.vz.security.sso.utils

import akka.actor.{ActorRef, ActorSystem}
import akka.cluster.routing.{ClusterRouterGroup, ClusterRouterGroupSettings}
import akka.pattern.ask
import akka.routing.RandomGroup
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.common.config.ConfigLoaderCore._
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.GenericMapRequest
import com.vz.security.sso.workflowmanager.ExpressionParsingUtil

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class FlowManager(etniActorRef: Option[ActorRef] = None, vcvsActorRef: Option[ActorRef] = None)(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) {

  val logger = new VZMapLogger(this)

  implicit val timeout: Timeout = askTimeout.millisecond

  val chainArray = route.split(":")
  val gatePassMap = gatePass()
  val conditionCheckMap = conditionCheck()

  def gatePass(): Map[String, ActorRef] = {
    val etniActor =
      if (etniActorRef.isDefined) etniActorRef.get
      else {
        system.actorOf(
          ClusterRouterGroup(RandomGroup(Nil),
            ClusterRouterGroupSettings(
              totalInstances = ConfigLoaderCore.totalNoOfInstancesEtni,
              routeesPaths = List(etniActorPath),
              allowLocalRoutees = ConfigLoaderCore.localroutesetni,
              useRoles = Set(servicesRole))).props())
      }

    val vcvsActor = if (vcvsActorRef.isDefined) vcvsActorRef.get
    else {
      system.actorOf(
        ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
          totalInstances = ConfigLoaderCore.totalNoOfInstancesvcvs, routeesPaths = List(vcvsActorPath),
          allowLocalRoutees = ConfigLoaderCore.localroutesvcvs, useRoles = Set(servicesRole))).props())

    }
    Map(ETNI -> etniActor, VCVS -> vcvsActor)
  }

  def conditionCheck(): Map[String, Map[String, Any] => Boolean] = {
    val gatewayCondition = eqFunction(gatewayKey, gatewayValueList)
    val etniCondition = ExpressionParsingUtil.compileWhereExpression(etniExpression)
    val vcvsCondition = eqFunction(vcvsKey, vcvsvalueList)
    Map(GATEWAY -> gatewayCondition, ETNI -> etniCondition, VCVS -> vcvsCondition)
  }

  /**
    * Generates function which equates value in given input Map[String, Any] to passed value
    * and gives out Boolean output
    *
    * @param key       Key to fetch from input map
    * @param valueList To evaluate value present in valueList
    * @return True/False
    */
  def eqFunction(key: String, valueList: List[String]): Map[String, Any] => Boolean = {
    val fn: Map[String, Any] => Boolean = map => {
      valueList.contains(map.getOrElse(key, "").toString.toLowerCase)
    }
    fn
  }

  /**
    *
    * @param requestMap     Final map from sender
    * @param originalSender original sender actor ref
    * @param senderName     Name of the module calling this function
    * @return string message to represent action
    */
  def forwardToNext(requestMap: Map[String, String], originalSender: ActorRef, senderName: String): String = {
    try {
      val forwardNext: Boolean = conditionCheckMap(senderName)(requestMap)
      val index = chainArray.indexOf(senderName)
      val result = if (forwardNext && chainArray.size > index + 1) {
        val nextRef = gatePassMap(chainArray(index + 1))
        nextRef.tell(GenericMapRequest(requestMap), originalSender)
        "Forwarded"
      }
      else {
        originalSender ! GenericMapRequest(requestMap)
        "Replied"
      }
      val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
      logmap += ("senderName" -> senderName)
      logmap += ("request" -> requestMap)
      logmap += ("result" -> result)
      logger.debug("forwardToNext", "NoSessionId", logmap)
      result
    }
    catch {
      case ex: Exception =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("senderName" -> senderName)
        logmap += ("request" -> requestMap)
        logger.error("forwardToNext", "NoSessionId", ex, logmap)
        "Error"
    }
  }

  /**
    *
    * @param requestMap Final map from sender
    * @param senderName Name of the module calling this function
    * @return Returns future of response by initiating chain
    */
  def initiateFlow(requestMap: Map[String, String], senderName: String): Future[Any] = {
    {
      val forwardNext: Boolean = conditionCheckMap(senderName)(requestMap)
      val index = chainArray.indexOf(senderName)
      if (forwardNext && chainArray.size > index + 1) {
        val nextRef = gatePassMap(chainArray(index + 1))
        nextRef ? GenericMapRequest(requestMap)
      }
      else {
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("senderName" -> senderName)
        logmap += ("request" -> requestMap)
        logmap += ("status" -> "condition failed or no chain left")
        logger.debug("initiateFlow", "NoSessionId", logmap)
        Future {
          GenericMapRequest(Map[String, String]())
        }
      }
    } recover {
      case ex: Exception =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "Exception while calling service actor")
        logmap += ("request" -> requestMap)
        logmap += ("action" -> "Dropping message")
        logmap += ("senderName" -> "senderName")
        logger.error("initiateFlow", "sessionNotDefined", ex, logmap)
        GenericMapRequest(Map[String, String]())
    }
  }

}
