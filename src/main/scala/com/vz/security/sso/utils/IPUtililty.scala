package com.vz.security.sso.utils

object IPUtililty {

  val ValidIPPattern: String = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])"

  def ipToLong(ipAddress:String):Long = {
    val ipAddressInArray  = ipAddress.split("\\.")
    var result:Long = 0
    if(isValidIP(ipAddress)) {
      for (i <- 0 until ipAddressInArray.length) {
        val power = 3 - i
        val ip:Int =  ipAddressInArray(i).toInt
        result += (ip * Math.pow(256, power)).toLong
      }
    } else {
      result += 0
    }
   result
  }

  def longToIP(ip:Long):String = {
    ((ip >> 24) & 0xFF) + "."+((ip >> 16) & 0xFF) + "."+ ((ip >> 8) & 0xFF) + "." + (ip & 0xFF)
  }

  def isValidIP(ip:String) = ip.matches(ValidIPPattern)
}