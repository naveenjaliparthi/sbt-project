package com.vz.security.sso.objects

import com.vz.security.sso.objects.listTypes.listTypes

/**
  * sample json line
  * {"geo_id":6252001,"network":"1.0.8.0/21","country_iso_code":"US","network_id":"8193",
  * "first_ip":"1.0.8.0","last_ip":"1.0.15.255","data_type":"new"}
  */

case class IPCountryInfo(countryCode:String, network:String, rangeStart:Long, rangeEnd:Long,
                         stateCode:Option[String] = Some("notProvided"),
                         cityName: Option[String] = Some("notProvided"),
                         latitude: Option[Double] = Some(0), longitude: Option[Double] = Some(0),
                         networkId:Long)

case class IPCountryRow(geo_id:Long, network:String, country_iso_code:String,
                        network_id:Long, first_ip:String, last_ip:String,
                        data_type:Option[String] = Some("new"), first_ip_id:Option[Long],
                        last_ip_id:Option[Long], state_code:Option[String], city_name:Option[String],
                        latitude: Option[Double], longitude: Option[Double])

case class IPRange(first_ip: Long, last_ip: Long, data_type: String, list_type: Option[String])

case class IPRangeStr(firstIP: String, lastIP: String, listType: listTypes, action: String)