
scredis {

  redis {
    # Redis server address
    host = ""

    # Redis server port
    port = ""

    # List of hosts or host:port tuples, which serve as seed nodes for a Redis cluster connection.
    # If port is missing, the configured port is used as default.
    #cluster-nodes = [${scredis.redis.host}":"${scredis.redis.port}]
    cluster-nodes = [""]

    # Redis server password (optional)
    # password = foobar

    # Database to be selected when connection is established
    database = 0

    # Name of this connection (optional). Setting this parameter will have the client send a
    # CLIENT SETNAME (available since 2.6.9) command just after having established the connection
    # name =
  }

  io {
    # Maximum amount of time allowed to establish a connection to the Redis server
    connect-timeout = 2 seconds

    # Maximum amount of time allowed to receive a batch of responses from the Redis server. A batch
    # of responses can contain as little as one response and as many as thousands of responses.
    receive-timeout = 5 seconds

    # Maximum amount of bytes to be sent to the Redis server at once. This controls the level of
    # request pipelining
    max-write-batch-size = 50000

    # Provides a hint to the underlying operating system of the size to allocate to the TCP send
    # buffer, in bytes
    tcp-send-buffer-size-hint = 5000000

    # Provides a hint to the underlying operating system of the size to allocate to the TCP receive
    # buffer, in bytes
    tcp-receive-buffer-size-hint = 500000

    akka {
      # Name of the actor system created by the Redis.scala instance.
      actor-system-name = "scredis"

      # Path to the definition of the io dispatcher used by the IOActor
      io-dispatcher-path = "scredis.io.akka.io-dispatcher"

      # Path to the definition of the listener dispatcher used by the ListenerActor
      listener-dispatcher-path = "scredis.io.akka.listener-dispatcher"

      # Path to the definition of the decoder dispatcher used by the DecoderActors
      decoder-dispatcher-path = "scredis.io.akka.decoder-dispatcher"

      io-dispatcher {
        executor = "thread-pool-executor"
        type = PinnedDispatcher
      }

      listener-dispatcher {
        executor = "thread-pool-executor"
        type = PinnedDispatcher
      }

      decoder-dispatcher {
        mailbox-type = "akka.dispatch.BoundedMailbox"
        mailbox-capacity = 1024
        throughput = 1024
      }
    }

    cluster {

      # TODO determine good values for these wait durations

      # How long to wait after TRYAGAIN error before retrying a send.
      try-again-wait = 10 milliseconds

      # How long to wait after a CLUSTERDOWN message before retrying a send.
      clusterdown-wait = 100 milliseconds
    }
  }

  # Defines global parameters, i.e. applied to all Clients together.
  # These parameters can only be modified in application.conf and will apply to all clients, no
  # matter how they were initialized or configured.
  global {
    # Maximum number of overall concurrently processing requests. This parameter directly
    # influences the memory consumption of scredis. Reducing it too much will impact performance
    # negatively.
    max-concurrent-requests = 30000

    # Configures the buffer pool used to encode requests.
    # The pool reduces garbage collection overhead.
    encode-buffer-pool {
      # Maximum capacity of the buffer pool, i.e. maximum number of buffers to pool
      pool-max-capacity = 30500

      # Maximum size of a buffer, in bytes. If more than 'buffer-max-size' bytes are required to
      # encode a request, the allocated buffer will not be pooled and thus garbage collected.
      buffer-max-size = 5000
    }
  }

}

custom-downing {
  stable-after = 60s
  down-removal-margin = off

  leader-auto-downing-roles {
    target-roles = []
  }
  role-leader-auto-downing-roles {
    leader-role = ""
    target-roles = []
  }
  oldest-auto-downing {
    oldest-member-role = ""

    down-if-alone = true

    # Shutdown ActorSystem on split brain resolution.
    # Setting to be true is favored in aspect of "fail fast".
    # If set to be false, only cluster will be shutdown and ActorSystem is preserved. Mainly for test purpose.
    shutdown-actor-system-on-resolution = true
  }
  majority-leader-auto-downing {
    majority-member-role = ""

    # Shutdown ActorSystem on split brain resolution.
    # Setting to be true is favored in aspect of "fail fast".
    # If set to be false, only cluster will be shutdown and ActorSystem is preserved. Mainly for test purpose.
    shutdown-actor-system-on-resolution = true
    down-if-in-minority = true
  }
  quorum-leader-auto-downing {
    role = ""
    quorum-size = 0
    down-if-out-of-quorum = true

    # Shutdown ActorSystem on split brain resolution.
    # Setting to be true is favored in aspect of "fail fast".
    # If set to be false, only cluster will be shutdown and ActorSystem is preserved. Mainly for test purpose.
    shutdown-actor-system-on-resolution = true
  }
}

elastic-stream {
  sinkBufferSize = 1000
  sinkMaxRetries = 5
  sinkRetryIntervalInSec = 1
}
risk-scores{
dmnDefaultRiskScore = 5
dmnDefaultReasonName = DMN_DECISION_NOT_FOUND
}
akka-stream {
  minBackoff = 30
  maxBackoff = 60
  randomFactor = 0.2
  wakeupTimeout = 3
  maxWakeups = 10
  producerParallelism = 200
  producerCloseTimeoutInSec = 60
  parallelism = 200
}

call-flow {
  askTimeout = 500 //In Millis
  vcvs = "vcvs"
  etni = "etni"
  gateway = "gateway"
  modelhost = "modelhost"
  initialRiskScore = "initialRiskScore"
  genericRiskScore = "genericRiskScore"
  kafka = "kafka"
  persistenceShard = "persistenceShard"
  aaPersistenceShard = "aaPersistenceShard"
  rtStreamShard = "rtStreamShard"
  aaRTStreamShard = "aaRTStreamShard"
  route = "gateway:etni:vcvs"
  etni-actor-path = "/user/etniActorSupervisor/etniActor"
  vcvs-actor-path = "/user/vcvsActorSupervisor/vcvsActor"
  ml-actor-path = "/user/lfActorSupervisor/lfActor"
  rules-app-path = "/user/getinitialriskscore"
  generic-rules-actor-path = "/user/genericriskscore"
  role = "services"
  mlRole = "modelhost"
  staticRole = "static"
  gatewayKey = "dnis"
  gatewayValue = "1010"
  etniKey = "omdnStatus"
  etniValue = "ai"
  etniExpression = "(~(etni:=:Y)~)~||~((~(etni:=:N)~)~&&~(~(omdnStatus:=:PO)~~||~~(omdnStatus:=:NH)~))"
  vcvsKey = "result"
  vcvsvalue = "yes"
  total-instances-etni = 10000
  allow-Local-Routees-etni = false
  total-instances-vcvs = 10000
  allow-Local-Routees-vcvs = false
  total-instances-ml = 10000
  allow-Local-Routees-ml = false
  total-instances-static = 10000
  allow-Local-Routees-static = false
}

work-flow {
  intopic = "work_flow_updates"
  generic-shard-name = "igpersistenceshard"
  generic-shard-role = "igpersistenceshard"
  number-of-generic-shards = 40
  rt-shard-name = "rtqueryshard"
  rt-shard-role = "rtqueryshard"
  number-of-rt-shards = 40
  kafkaTopicsKey = "kafkaTopics"
  rtQIdKey = "queryId"
  rtShardKey = "shard"
  rtParKey = "parallelism"
  wf-log-timer = 240 //In Mins
  askTimeout = 2000 //In Millis
  metadataKeys = "module,wfId,wfNodeId"
}
toolbox {
  opRegex = """[0-9a-zA-Z-#()_/-/.! ]+"""
  exprRegex = """\(([0-9a-zA-Z:><=._/!-/ ]+)\)"""
}

elastic {
  enable-https = false
  host = "vpc-vz-bvhv-vz-bvhv-nonprod-5ixshvnfdpcfvne2nfbjlbfhr4.us-east-1.es.amazonaws.com"
  host = ${?ELASTIC_HOST}
  port = 80
  port = ${?ELASTIC_PORT}
  wfgindex = "wfm_index"
  wfgtype = "current"
  //Cache manager
  cacheConfigIndex = "managed_caches"
  cacheConfigType = "_doc"
  mangoIndex = "hashed_cache_data"
  mangoType = "_doc"
  rangeIndex = "range_cache_snapshots"
  rangeType = "_doc"
}


vcm{
  encryption{
    profTokenEKey = "profileTokenEncryptionKey"
    profTokenSKey = "profileTokenSigningEncryptionKey"
    profTokenICount = 65536
    pairDelimiter = ";"
    kvDelimiter = ":"
  }
}
